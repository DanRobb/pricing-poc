const {
  calculateSharePortionByDateAction,
  assembleAlgorithmSegments,
  dryRunSharePortionAction,
} = require("./share-portion/sharePortionCore");

const { algorithmSegmentRegistry, customFactorBuilder } = require("./algorithm/algorithmComponents");

const { factorSets } = require("./config/ms3/factors");

const { getFactorSetByDateAndName, createSegmentFactor } = require("./config/factorSetUtils");

// ******************* Exercise It *********************
let quote = calculateSharePortionByDateAction({
  coverageDate: new Date(2021, 07, 15),
  dateOfBirth: new Date(1980, 05, 15),
  maritalStatus: "M",
  tier: "Family",
  program: "MS3",
  programLevel: "3k",
  state: "MN",
  hid: true,
  mdf: true,
});
console.log(`Dynamic Quote: ${quote}`);
console.log("");
console.log("*************");
console.log("");

let quote2 = calculateSharePortionByDateAction({
  coverageDate: new Date(2021, 07, 15),
  dateOfBirth: new Date(1980, 05, 15),
  maritalStatus: "U",
  tier: "Single",
  program: "MS3",
  programLevel: "3k",
  state: "MN",
  hid: true,
  mdf: true,
});
console.log(`Dynamic Quote: ${quote2}`);
console.log("");
console.log("*************");
console.log("");

let quote3 = calculateSharePortionByDateAction({
  coverageDate: new Date(2020, 07, 15),
  dateOfBirth: new Date(1980, 05, 15),
  maritalStatus: "M",
  tier: "Family",
  program: "MS3",
  programLevel: "3k",
  state: "MN",
  hid: true,
  mdf: true,
});
console.log(`Dynamic Quote: ${quote3}`);
console.log("");
console.log("*************");
console.log("");

// for (let index = 0; index < 100000; index++) {
let quote4 = calculateSharePortionByDateAction({
  coverageDate: new Date(2020, 07, 15),
  dateOfBirth: new Date(1980, 05, 15),
  maritalStatus: "U",
  tier: "Single",
  program: "MS3",
  programLevel: "3k",
  state: "MN",
  hid: true,
  mdf: true,
  verbose: false,
});

console.log(`Dynamic Quote: ${quote4}`);
console.log("");
console.log("*************");
console.log("");
// }

let quote5 = "NOT YET IMPLEMENTED";
console.log(quote5);

// ******************* Dry Run Setup and Execution *********************
console.log();
console.log("************************** DRY RUN **************************");
console.log();

const dryRunAlgoComponents = [
  "baseRateFactor",
  "ageFactor",
  "maritalStatusFactor",
  "tierFactor",
  "ahpAgeFactor",
  "geoFactor",
  "cbsaFactor",
  "healthFactor",
];

const myAlgo = assembleAlgorithmSegments(dryRunAlgoComponents, algorithmSegmentRegistry, false);

const testFactorSet = [
  getFactorSetByDateAndName(factorSets, new Date(), "tierFactorSet"),
  getFactorSetByDateAndName(factorSets, new Date(), "geoFactorSet"),
  getFactorSetByDateAndName(factorSets, new Date(), "baseRateFactorSet"),
  getFactorSetByDateAndName(factorSets, new Date(), "ageFactorSet"),
  getFactorSetByDateAndName(factorSets, new Date(), "ahpAgeFactorSet"),
  getFactorSetByDateAndName(factorSets, new Date(), "cbsaFactorSet"),
  getFactorSetByDateAndName(factorSets, new Date(), "healthFactorSet"),
  // Changing the default values here...
  createSegmentFactor("maritalStatusFactorSet", [
    ["U", 0.8],
    ["M", 1.01],
  ]),
];

console.log(
  "Note the slightly different results from previous (non dry run) call after marital status is applied and the decreased final value"
);
console.log();
console.log(
  "Dynamic Quote: ",
  dryRunSharePortionAction(myAlgo, testFactorSet, {
    coverageDate: new Date(2020, 07, 15),
    dateOfBirth: new Date(1980, 05, 15),
    maritalStatus: "U",
    tier: "Single",
    program: "MS3",
    programLevel: "3k",
    state: "MN",
    hid: true,
    mdf: true,
  })
);

// ******************* Custom Component Dry Run Setup and Execution *********************
console.log();
console.log("************************** Custom Component DRY RUN **************************");
console.log();

const customComponentSet = [
  "baseRateFactor",
  "ageFactor",
  "maritalStatusFactor",
  "tierFactor",
  "ahpAgeFactor",
  "geoFactor",
  "cbsaFactor",
  "healthFactor",
];

const customAlgo = assembleAlgorithmSegments(customComponentSet, algorithmSegmentRegistry, false);

const dansCustomFactorThatIsAwesome = customFactorBuilder(
  "dansCustomFactorThatIsAwesome",
  "tupleLookupStrategy",
  "multiplicativeFactorStrategy"
);

const enhancedAlgo = [...customAlgo, dansCustomFactorThatIsAwesome];

const customFactorSet = [
  getFactorSetByDateAndName(factorSets, new Date(), "tierFactorSet"),
  getFactorSetByDateAndName(factorSets, new Date(), "geoFactorSet"),
  getFactorSetByDateAndName(factorSets, new Date(), "baseRateFactorSet"),
  getFactorSetByDateAndName(factorSets, new Date(), "ageFactorSet"),
  getFactorSetByDateAndName(factorSets, new Date(), "maritalStatusFactorSet", new Date()),
  getFactorSetByDateAndName(factorSets, new Date(), "ahpAgeFactorSet"),
  getFactorSetByDateAndName(factorSets, new Date(), "cbsaFactorSet"),
  // getFactorSetByDateAndName(factorSets, new Date(), "cbsaFactorSet"),
  getFactorSetByDateAndName(factorSets, new Date(), "healthFactorSet"),
];

// note the requirement for 'set' here and the parity between the set name and the factor name.  Kinda clunky.
const dansCustomFactorThatIsAwesomeSet = createSegmentFactor("dansCustomFactorThatIsAwesomeSet", [
  ["A", 1.01],
  ["B", 0.99],
]);

const enhancedFactorSet = [...customFactorSet, dansCustomFactorThatIsAwesomeSet];

console.log("Note the application of the additional custom factor and factor values, as well as the difference ");
console.log("between the dynamic and the 'old way' (which doesn't support custom factors) final values");
console.log();

console.log(
  "Dynamic Quote: ",
  dryRunSharePortionAction(enhancedAlgo, enhancedFactorSet, {
    coverageDate: new Date(2020, 07, 15),
    dateOfBirth: new Date(1975, 05, 15),
    maritalStatus: "U",
    tier: "Single",
    program: "MS3",
    programLevel: "3k",
    state: "MN",
    mdf: true,
    hid: true,
    custom1: "B",
  })
);

console.log();
console.log("*************************** New Program: CoShare *****************************");
console.log();

console.log(
  "Dynamic Quote: ",
  dryRunSharePortionAction(enhancedAlgo, enhancedFactorSet, {
    coverageDate: new Date(2020, 07, 15),
    dateOfBirth: new Date(1975, 05, 15),
    maritalStatus: "U",
    tier: "Single",
    program: "CS1",
    programLevel: "3k",
    state: "MN",
    mdf: true,
    hid: true,
    custom1: "B",
  })
);
