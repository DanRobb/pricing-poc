# Demo.md

## Walk through index.js

### Explain what's being imported. Point out the pieces that relate to factors, those that relate to algorithms and the byDate and dry run.

### Stop at custom components and dive in to SharePortionCore

## SharePortionCore...

### Quickly review imports for "general purpose"

### Show the by Date and the dry run functions.

### Point out the nexus of dry run and by date and how one logically picks up after the other.

## Show quickly the algorithm components

## Show quickly the factor sets, but emphasize that their data format in that file is still in flux (hence no DB) and that really they're just the same values we have today.

## Bounce back to index.js and show the custom factors
