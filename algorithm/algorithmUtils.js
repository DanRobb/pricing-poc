const {
  filterObjectArrayByValidityDates,
  curry,
  filterArrayOfObjectsByNamedPropAndValue,
} = require("../core-utils/baseUtils");

const getAlgorithmSegmentByName = (implementations, name) => {
  const implementation = implementations.find((obj) => {
    return obj.algorithmComponentName === name;
  });
  return implementation;
};

const resolveAlgorithmSegmentsByDate = (algorithmConfigurations, program, coverageDate) => {
  const programConfig = filterArrayOfObjectsByNamedPropAndValue(algorithmConfigurations, "program", program)[0];
  const d = filterObjectArrayByValidityDates(programConfig.algorithmConfigurations, coverageDate);
  return d[0].componentSet;
};

const createAlgorithmSegment = (componentName, implementation, filter) => {
  return {
    algorithmComponentName: componentName,
    functionImplementation: implementation,
    parameterFilter: filter,
  };
};

const applyMultiplicativeFactor = (multiplicand, multiplier) => {
  return multiplicand * multiplier;
};

const applyAdditiveFactor = (base, addend) => {
  return base + addend;
};

const loggingWrappedSegment = (f) => {
  wrapper = (a, b, c) => {
    console.log();
    console.log("********************************************************************************");
    console.log();
    console.log(`   Beginning new factor evaluation for ${f.name}....`); // this is gonna break.
    console.log();
    console.log("      Current Quote Value is: ", c);
    console.log();
    console.groupCollapsed(`      Calling function with provided factor set:`);
    // we always have factor sets now.
    console.log("      ", a);
    console.groupEnd();
    console.log();
    console.groupCollapsed("      ... and supplied parameters: ");
    console.log("      ", b);
    console.groupEnd();
    console.log();

    // curry relies on the number of params in this apply call to know when to return.  Can't just use rest here.
    const val = f.apply(this, [a, b, c]);

    console.log(`   result was ${val} After Applying factor`, f.name); // this is gonna break.
    console.log();
    console.log("********************************************************************************");
    console.log();
    return val;
  };
  return wrapper;
};

const curryAlgorithmSegments = (algorithmSegments) => {
  return algorithmSegments.map((segment) => {
    return createAlgorithmSegment(
      segment.algorithmComponentName,
      curry(segment.functionImplementation),
      segment.parameterFilter
    );
  });
};

module.exports = {
  getAlgorithmSegmentByName,
  resolveAlgorithmSegmentsByDate,
  createAlgorithmSegment,
  applyMultiplicativeFactor,
  applyAdditiveFactor,
  loggingWrappedSegment,
  curryAlgorithmSegments,
};
