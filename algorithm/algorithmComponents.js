const {
  curry,
  pick,
  determineAgeBasedOnDOB,
  filterArrayOfObjectsByNamedPropAndValue,
} = require("../core-utils/baseUtils");

const { createAlgorithmSegment, applyMultiplicativeFactor, applyAdditiveFactor } = require("./algorithmUtils");

/**
 * Interestingly, baseRateFactor doesn't depend on any external parameters, so it varies from
 * standard multiplicative parameters.
 */
const baseRateFactor = (factorSet, params, quote) => {
  return applyAdditiveFactor(quote, factorSet.factorValues[0]);
};

/**
 */
const ageFactor = (factorSet, params, quote) => {
  const factor = rangeFactorLookup(
    factorSet.factorValues,
    determineAgeBasedOnDOB(params.dateOfBirth, params.coverageDate)
  );
  return applyMultiplicativeFactor(quote, factor);
};

const tierFactor = (factorSet, params, quote) => {
  const factor = tupleFactorLookup(factorSet.factorValues, params.tier);
  return applyMultiplicativeFactor(quote, factor);
};

/*
 * This is actually a state or region factor
 */
const geoFactor = (factorSet, params, quote) => {
  const factor = tupleFactorLookup(factorSet.factorValues, params.state);
  if (factor === undefined || factor === null) {
    return applyMultiplicativeFactor(quote, 1.0);
  }
  return applyMultiplicativeFactor(quote, factor);
};

const maritalStatusFactor = (factorSet, params, quote) => {
  let maritalStatusFactor = tupleFactorLookup(factorSet.factorValues, params.maritalStatus);
  return applyMultiplicativeFactor(quote, maritalStatusFactor);
};

const ahpAgeFactor = (factorSet, params, quote) => {
  const age = determineAgeBasedOnDOB(params.dateOfBirth, params.coverageDate);
  const program = `${params.program}-${params.programLevel}`;

  const selectedProgramFactors = factorSet.factorValues.filter((row) => {
    return row.programName === program;
  })[0];

  // FIXME: This is getting progressively more ugly as we infer meaning based on ordinal position and data type.
  // FIXME: This is a little brittle.  factorValues[0] will be an array if normal program or a string if redirected.
  // target.
  if (Array.isArray(selectedProgramFactors.factorValues[0])) {
    return applyStandardAhpFactor(selectedProgramFactors.factorValues, age, quote);
  } else {
    const targetFactors = switchToTargetFactors(factorSet, selectedProgramFactors.factorValues[0]);
    const standardQuote = applyStandardAhpFactor(targetFactors.factorValues, age, quote);
    const coShareFactor = selectedProgramFactors.factorValues[1];
    return applyCoShareFactor(standardQuote, coShareFactor);
  }
};

const switchToTargetFactors = (factorSet, selectedProgramFactors) => {
  const targetFactors = factorSet.factorValues.filter((rowObj) => {
    return rowObj.programName === selectedProgramFactors;
  });
  return targetFactors[0];
};

const applyCoShareFactor = (initialQuote, selectedProgramFactors) => {
  let final = applyMultiplicativeFactor(initialQuote, selectedProgramFactors);
  return final;
};

const applyStandardAhpFactor = (selectedProgramFactors, age, quote) => {
  const factor = rangeFactorLookup(selectedProgramFactors, age);
  return applyMultiplicativeFactor(quote, factor);
};

/**
 * This is actually the health incentive discount and is a flat 5%.
 *
 * @param {*} factorSet
 * @param {*} doesApply
 * @param {*} quote
 * @returns
 */
const healthFactor = (factorSet, params, quote) => {
  if (params.hid) {
    return applyMultiplicativeFactor(quote, 0.95);
  }
  return applyMultiplicativeFactor(quote, 1.0);
};

/*
 * Core Based Statistical Area:  https://en.wikipedia.org/wiki/Core-based_statistical_area
 * Actually just broken by 3 digit zip code, but design enables 5 digits if desired
 */
const cbsaFactor = (factorSet, params, quote) => {
  const param = params.cbsaFactor;
  return quote * 1.0;
};

/**
 * This is a duplicate of the CBSA factor.
 *
 * @param {*} factorSet
 * @param {*} params
 * @param {*} quote
 * @returns
 */
const applyMarketingDiscountFactor = (factorSet, params, quote) => {
  if (params.mdf) {
    return applyMultiplicativeFactor(quote, 0.9);
  }
  return applyMultiplicativeFactor(quote, 1.0);
};

/**
 * name of component, lookup strategy, calculation strategy and a value to apply in that calc strat.
 */
const customFactorBuilder = (name, lookupStrategy, calcStrategy) => {
  calcStrategy = filterArrayOfObjectsByNamedPropAndValue(calculationStrategyRegistry, "strategyName", calcStrategy)[0]
    .implementation;
  // I really hate filtering arrays and getting arrays back.  So many [0] that I shouldn't need.
  const strategy = filterArrayOfObjectsByNamedPropAndValue(lookupStrategyRegistry, "strategyName", lookupStrategy)[0]
    .implementation;
  const algorithmComponent = (factorSet, params, quote) => {
    // the need to call factor values here stumped me for a second and that call is repeated in
    // each factor as well.  May be a reason to rely on factor builder for ALL factor types.  Not sure.
    const customFactor = strategy(factorSet.factorValues, params.custom1);
    return calcStrategy(quote, customFactor);
  };
  // custom components don't yet support parameter validation.  Note below arrow function...
  const customFactorParameterValidator = (params) => params;

  return createAlgorithmSegment(name, algorithmComponent, customFactorParameterValidator);
};

/**
 * Ok, so one problem here is what happens when factorSets length and searchKey length differ.
 *
 * @param {*} factorSet
 * @param {*} searchKeys
 * @returns
 */
const tupleFactorLookup = (factorSet, searchKeys) => {
  factorValue = factorSet.find((set) => {
    return set.find((entry) => {
      return entry === searchKeys;
    });
  });
  return factorValue && factorValue[factorValue.length - 1]; // NO POP!!  POP MUTATES THE ARRAY!
};

const rangeFactorLookup = (factorSet, searchKeys) => {
  const factor = factorSet.find((row) => {
    return row[0] <= searchKeys && row[1] >= searchKeys;
  });
  return factor[2];
};

const parameterFilter = (selectedFields, params) => {
  const out = pick(params, selectedFields);
  const outLen = Object.entries(out).length;
  // technically this is providing 2 functions.  One is the picking so that later logging is more narrowly scoped.
  // the second is parameter validation, which logically should be separate.
  if (outLen !== selectedFields.length) {
    throw new Error(`Missing required parameter(s): ${selectedFields}`);
  }
  return out;
};

const getParameterFilter = (selectedFields) => {
  const func = curry(parameterFilter);
  return func(selectedFields);
};

const algorithmSegmentRegistry = [
  createAlgorithmSegment("baseRateFactor", baseRateFactor, getParameterFilter([])),
  createAlgorithmSegment("ageFactor", ageFactor, getParameterFilter(["dateOfBirth", "coverageDate"])),
  createAlgorithmSegment("maritalStatusFactor", maritalStatusFactor, getParameterFilter(["maritalStatus"])),
  createAlgorithmSegment("tierFactor", tierFactor, getParameterFilter(["tier"])),
  createAlgorithmSegment(
    "ahpAgeFactor",
    ahpAgeFactor,
    getParameterFilter(["coverageDate", "dateOfBirth", "program", "programLevel"])
  ),
  createAlgorithmSegment("geoFactor", geoFactor, getParameterFilter(["state"])),
  // requires boolean true or false.  Awkward how this is used in the front end.
  createAlgorithmSegment("healthFactor", healthFactor, getParameterFilter(["hid"])),
  // Needs to require zip + coverage date.  Currently implemented as simple boolean...
  // Probably belongs outside the core procing algorithm anyway.
  createAlgorithmSegment("cbsaFactor", cbsaFactor, getParameterFilter(["mdf"])),
  createAlgorithmSegment("marketingDiscountFactor", applyMarketingDiscountFactor, getParameterFilter(["mdf"])),
];

const lookupStrategyRegistry = [
  { strategyName: "tupleLookupStrategy", implementation: tupleFactorLookup },
  { strategyName: "rangeLookupStrategy", implementation: rangeFactorLookup },
];

const calculationStrategyRegistry = [
  { strategyName: "multiplicativeFactorStrategy", implementation: applyMultiplicativeFactor },
  { strategyName: "additiveFactorStrategy", implementation: applyAdditiveFactor },
];

// TODO: remove after demonstration.
const nukeOldWay = [baseRateFactor, ageFactor, maritalStatusFactor, tierFactor, ahpAgeFactor, geoFactor, healthFactor];

module.exports = { algorithmSegmentRegistry, customFactorBuilder, nukeOldWay }; // TODO: remove after demonstration.
