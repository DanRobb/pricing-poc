const algorithmConfigurations = [
  {
    program: "MS3",
    algorithmConfigurations: [
      {
        validityDates: { startDate: new Date("2018-06-01"), endDate: new Date("2019-05-30") },
        componentSet: [
          "baseRateFactor",
          "ageFactor",
          "maritalStatusFactor",
          "tierFactor",
          "ahpAgeFactor",
          "geoFactor",
          "cbsaFactor",
          "healthFactor",
        ],
      },
      {
        validityDates: { startDate: new Date("2019-06-01"), endDate: new Date("2020-05-30") },
        componentSet: [
          "baseRateFactor",
          "ageFactor",
          "maritalStatusFactor",
          "tierFactor",
          "ahpAgeFactor",
          "geoFactor",
          "cbsaFactor",
          "healthFactor",
        ],
      },
      {
        validityDates: { startDate: new Date("2020-06-01"), endDate: new Date("2021-05-30") },
        componentSet: [
          "baseRateFactor",
          "ageFactor",
          "maritalStatusFactor",
          "tierFactor",
          "ahpAgeFactor",
          "geoFactor",
          "cbsaFactor",
          "healthFactor",
        ],
      },
      {
        validityDates: { startDate: new Date("2021-06-01"), endDate: new Date("2022-05-30") },
        componentSet: [
          "baseRateFactor",
          "ageFactor",
          "maritalStatusFactor",
          "tierFactor",
          "ahpAgeFactor",
          "geoFactor",
          "cbsaFactor",
          "healthFactor",
          // "marketingDiscountFactor",
        ],
      },
    ],
  },
];

module.exports = { algorithmConfigurations };
