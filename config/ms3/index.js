const { algorithmConfigurations } = require("./algorithm-segments");
const { factorSets } = require("./factors");

module.exports = { algorithmConfigurations, factorSets };
