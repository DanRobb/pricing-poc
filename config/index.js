const { algorithmConfigurations, factorSets } = require("./ms3");

module.exports = { algorithmConfigurations, factorSets };
