const { filterObjectArrayByValidityDates, filterArrayOfObjectsByNameProp } = require("../core-utils/baseUtils");

const createSegmentFactor = (name, factorValues) => {
  return {
    name,
    factorValues,
  };
};

/**
 * Returns all factor sets valid for the given date.
 *
 * @param {*} validityDate
 * @returns
 */
const resolveFactorSetsByDate = (factorSetConfigs, validityDate) => {
  const filteredFactorSetConfigs = filterFactorSetConfigsByDate(factorSetConfigs, validityDate).map((set) => {
    return createSegmentFactor(set.name, set.factorRangeSets[0].factorValues);
  });

  return filteredFactorSetConfigs;
};

const filterFactorSetConfigsByDate = (factorSets, validityDate) => {
  const dateRestrictedFactorSets = factorSets.map((set) => {
    const factors = filterObjectArrayByValidityDates(set.factorRangeSets, validityDate);
    return {
      name: set.name,
      factorRangeSets: factors,
    };
  });

  return dateRestrictedFactorSets;
};

const getFactorSetByDateAndName = (factorSets, date, name) => {
  const factorSetsRestrictedByDate = resolveFactorSetsByDate(factorSets, date);
  const retVal = getFactorSetByName(factorSetsRestrictedByDate, name);

  return retVal;
};

const getFactorSetByName = (factorSetsRestrictedByDate, name) => {
  return filterArrayOfObjectsByNameProp(factorSetsRestrictedByDate, name)[0];
};

const getFactorSetForSegment = (factorSets, segment) => {
  return filterFactorSetsBySegment(factorSets, segment)[0];
};

const filterFactorSetsBySegment = (factorSets, segment) => {
  const factorName = `${segment.algorithmComponentName}Set`;
  const set = filterArrayOfObjectsByNameProp(factorSets, factorName);
  return set;
};

module.exports = {
  createSegmentFactor,
  resolveFactorSetsByDate,
  getFactorSetByName,
  getFactorSetByDateAndName,
  getFactorSetForSegment,
  filterFactorSetsBySegment,
};
