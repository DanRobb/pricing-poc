# These are topics left to cover and things that need doing.

## Core Share Portion Algorithm

- MR for pricing left
- MR for Flex introduction
- Jest tests for a handful of pieces.
- Break coshare and flex in to their own factors earlier than ageAhpFactor application (awkward. 1st attempt failed)
- Finish implementing MS2, Manna, and SA
- figure out what to do with "marketing-ish" factors and how they should tie in.
  - Do these factors get added / subtracted to share portion, admin, both or something else entirely (HID)?
- Fix duplicate ranges in a single factor set validation
- implement custom factor using rangelookup
- implement custom factors using additive calculation strategy

- custom factor sets do not support parameter validation (though they do support algorithm -> factor set validation)
- enable algorithm segment ordering / precedence

- implement standard factor with additive calculation strategy. (DONE)
- Enable undefined end dates for factor sets so that you don't have to continually update them if they never change. (DONE)
- Enable algorithm to factor validation (DONE)
- Enable Parameter to function component (or factor set) validation (DONE)
- Do a once over for overall code structure (DONE)
- Remove unnecessary code and comments (DONE)
- ensure variables and functions are named clearly (DONE)

## MS2 Pricing

- Need to be able to completely override the concept of a "base rate" for ms2 pricing. Implies tapping in
  to the algorithm lookup with something OTHER than just a coverage date. Also need program.
- Are algorithm lookups really program first, then by date range???

## Admin Portion

- Implement admin portion on a per product basis with calculation strategy flexibility

## Wrapping the admin and share portions in to a total.

## MS Flex pricing

- if it really is just 30% of MS3 pricing, and that's going to remain that way, I can do this in 7 lines of code.
- It really IS identical to MS3 pricing.

## SA Pricing

## SA+ Pricing

## Mana Pricing

## Handling of Discounts and other wonky factors that shouldn't be provided during a quote, but should be provided during a pricing event run.

## How does this "pricing engine" play in to monthly billing? Does it? Or should we store off the result of a price quote and recurringly bill that amount?

## Figure out the response format for the old pricing engine and get this response format in alignment
