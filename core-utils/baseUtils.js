// Utilities
const pipe =
  (...fns) =>
  (x) =>
    fns.reduce((v, f) => f(v), x);

function curry(func) {
  return function curried(...args) {
    if (args.length >= func.length) {
      return func.apply(this, args);
    } else {
      return function (...args2) {
        return curried.apply(this, args.concat(args2));
      };
    }
  };
}

/**
 * Can I use pick down in any of the below functions?
 *
 * @param {*} obj
 * @param {*} keyArray
 * @returns
 */
const pick = (obj, keyArray) => {
  return Object.keys(obj)
    .filter((key) => keyArray.indexOf(key) >= 0)
    .reduce((newObj, key) => Object.assign(newObj, { [key]: obj[key] }), {});
};

/**
 * Given an array of objects where those objects contain a property called "validityDates" and those dates are denoted by
 * 'startDate' and 'endDate', along with a target date, this function will return the first occurence of an object where the
 * supplied date falls between the start and end dates inclusive.
 *
 * @param {Array} arr An array of objects
 * @param {Date} date The date for which to search for in the validity ranges within the objects of the range.
 * @returns The object which contains a validity date range in which the supplied date falls.
 */
const filterObjectArrayByValidityDates = (arr, date) => {
  let obj = arr.filter((rowObj) => {
    const validityDates = rowObj.validityDates;
    return validityDates.startDate < date && (!validityDates.endDate || validityDates.endDate >= date);
  });

  return obj;
};

const filterArrayOfObjectsByNameProp = (arr, keyName) => {
  return filterArrayOfObjectsByNamedPropAndValue(arr, "name", keyName);
};

const filterArrayOfObjectsByNamedPropAndValue = (arr, namedProp, value) => {
  const filteredArray = arr.filter((obj) => {
    return obj[namedProp] === value;
  });
  return filteredArray;
};

const determineAgeBasedOnDOB = (dateOfBirth, coverageDate) => {
  return dateDiffInYears(dateOfBirth, coverageDate);
};

/**
 * Replace this with a Luxon function at some point.  Just trying to avoid using libraries for the moment.
 *
 * @param {Date} dateold The start date to count from
 * @param {Date} datenew The end date to count to
 * @returns The difference between the old and new dates in Years as an integer
 */
function dateDiffInYears(dateold, datenew) {
  var ynew = datenew.getFullYear();
  var mnew = datenew.getMonth();
  var dnew = datenew.getDate();
  var yold = dateold.getFullYear();
  var mold = dateold.getMonth();
  var dold = dateold.getDate();
  var diff = ynew - yold;
  if (mold > mnew) diff--;
  else {
    if (mold == mnew) {
      if (dold > dnew) diff--;
    }
  }
  return diff;
}

module.exports = {
  determineAgeBasedOnDOB,
  filterArrayOfObjectsByNameProp,
  filterArrayOfObjectsByNamedPropAndValue,
  filterObjectArrayByValidityDates,
  curry,
  pick,
};
