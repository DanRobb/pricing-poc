// TODO: remove after demonstration.

const {getFactorSetByName} = require("../config/factorSetUtils")
const {nukeOldWay} = require("../algorithm/algorithmComponents")

const [baseRateFactor, ageFactor, maritalStatusFactor, tierFactor, ahpAgeFactor, geoFactor, healthFactor] = nukeOldWay

/**
 * This function is a concrete implementation (the first one actually) of the pricing engine algorithm.  As you can see, 
 * it lacks dynamic functionality and can only work with the specified factors.  
 * 
 * This would be equivalent to the 'non curried' version of the pricing engine and highlights some of the limitations 
 * in that strategy, namely the rigidness and non configurability of the component factor segments.  Each version of 
 * the algorithm would have to specify a function similar to the below function in order to version the algorithm itself.  
 * 
 * @param {FactorSets} FactorSets - A complete set of factors for this run.  Not all factors for all of time.  
 * @param {Object} arguments - the user supplied parameters on which to base the quote.
 * @returns Number - A quote price given the supplied factor sets and arguments.
 */
const oldWay = (factorSets, arguments) => {
    let quotePrice = 0;
    quotePrice = baseRateFactor(getFactorSetByName(factorSets, "baseRateFactorSet"), arguments, quotePrice);
    quotePrice = ageFactor(getFactorSetByName(factorSets, "ageFactorSet"), arguments, quotePrice);
    quotePrice = maritalStatusFactor(getFactorSetByName(factorSets, "maritalStatusFactorSet"), arguments, quotePrice);
    quotePrice = tierFactor(getFactorSetByName(factorSets, "tierFactorSet"), arguments, quotePrice);
    quotePrice = ahpAgeFactor(getFactorSetByName(factorSets, "ahpAgeFactorSet"), arguments, quotePrice);
    quotePrice = geoFactor(getFactorSetByName(factorSets, "geoFactorSet"), arguments, quotePrice);
    quotePrice = healthFactor(getFactorSetByName(factorSets, "healthFactorSet"), arguments, quotePrice);
  
    console.log() 
    console.log(`Non Curried Final: ${quotePrice}`);
    console.log(`   ${arguments.coverageDate} MARRIED share portion is`)
  };

  module.exports = {oldWay}