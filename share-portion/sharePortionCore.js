const { oldWay } = require("./nonCurryMethod"); // TODO: remove after demonstration.
const {
  resolveFactorSetsByDate,
  getFactorSetForSegment,
  filterFactorSetsBySegment,
} = require("../config/factorSetUtils");
const {
  resolveAlgorithmSegmentsByDate,
  createAlgorithmSegment,
  getAlgorithmSegmentByName,
  loggingWrappedSegment,
  curryAlgorithmSegments,
} = require("../algorithm/algorithmUtils");
const { factorSets, algorithmConfigurations } = require("../config/");
const { algorithmSegmentRegistry } = require("../algorithm/algorithmComponents");

/**
 *
 * @returns {Number} SharePortion
 */
const calculateSharePortionByDateAction = (params) => {
  const { coverageDate, verbose, program } = params;

  const dateRestrictedAlgorithmConfiguration = resolveAlgorithmSegmentsByDate(
    algorithmConfigurations,
    program,
    coverageDate
  );

  const algorithmSegments = assembleAlgorithmSegments(
    dateRestrictedAlgorithmConfiguration,
    algorithmSegmentRegistry,
    verbose
  );

  // the baserate factor set isn't duplicated.  it's rangeFactor Set is.  We don't check for duplicate date ranges
  // on anything.
  const dateRestrictedFactorSet = resolveFactorSetsByDate(factorSets, coverageDate);

  const algorithm = assembleAlgorithm(algorithmSegments, dateRestrictedFactorSet);

  result = algorithm(params);
  return result;
};

/**
 *
 *
 * @param  {Any} customAlgorithmSegments This is the preconfigured algorithm list.  Needs to include string name and function.
 * @param {Any} customFactorSet This is the list of factors you want to use to generate a run.  Should be all factors values.
 * @param {Any[]} rest The remaining parameters supplied to do a share price lookup (DOB, Marital Status, etc.)
 */
const dryRunSharePortionAction = (customAlgorithmSegments, customFactorSet, params) => {
  const algorithm = assembleAlgorithm(customAlgorithmSegments, customFactorSet);
  result = algorithm(params);
  return result;
};

/**
 * Validates algorithm segments match factor sets
 * curries the algorithm segment implementations.
 * applies the factor sets to the curried implementations.
 * returns a function that takes user supplied parameters.
 *
 * @param {*} algorithmSegments
 * @param {*} factorSet
 * @returns
 */
const assembleAlgorithm = (algorithmSegments, factorSet) => {
  // this function doesn't return anything.  Violates FP principles.
  validateAlgorithm(algorithmSegments, factorSet);

  const curriedAlgorithmSegments = curryAlgorithmSegments(algorithmSegments);

  const curriedFactorSetAppliedAlgorithmSegments = applyFactorSets(curriedAlgorithmSegments, factorSet);

  const finalAlgorithm = (params) => {
    const finalAlgorithm = applyParams(curriedFactorSetAppliedAlgorithmSegments, params);

    const quote = finalAlgorithm.reduce(loggingWrappedReducer, 0);

    // TODO: remove after demonstration.  Only works for MS3 quotes because of hard coded algorithm and factor components.
    if (params.program === "MS3") {
      oldWay(factorSet, params);
    }

    return quote;
  };

  return finalAlgorithm;
};

const validateAlgorithm = (algorithmSegments, factorSet) => {
  // this is really a for each unless we turn this in to a boolean returning function (which we probably should)
  const doNotReturn = algorithmSegments.map((segment) => {
    // again, we're filtering, but presuming more of a 'find' semantic.
    const factorSetForAlgorithmSegment = filterFactorSetsBySegment(factorSet, segment);
    // test this twice.  Once with no factor set at all and another with the factor values attribute missing.
    if (factorSetForAlgorithmSegment.length > 1) {
      throw new Error(
        `Specified algorithm has multiple factor sets defined for the given time interval for algorithm component ${segment.algorithmComponentName}`
      );
    }
    // note the null safe navigator...
    if (factorSetForAlgorithmSegment[0]?.factorValues === undefined) {
      throw new Error(
        `Specified algorithm is missing required factor sets for algorithm component ${segment.algorithmComponentName}`
      );
    }

    // missing a check to see if you have a single factor with overlapping date ranges... variant of first check above.
  });
};

/**
 * Converts a string list of pre determined algorithm components in to their concrete implementations.
 *
 * @param {Any} dateRestrictedAlgorithmConfiguration An array of string algorithm component configurations to be included
 * @param {Any} availableFactors An array of available algorithm component factor functions
 * @returns An algorithm function segment consisting of a string identifier, the concrete implementation function
 *    that correlates with that string identifer, and a parameter filter that defines which parameter object attributes
 *    this concrete function relies on.
 */
const assembleAlgorithmSegments = (dateRestrictedAlgorithmConfiguration, algorithmSegmentRegistry, verbose) => {
  // This is a LITTLE bit messy.  We're doing the following:
  //   - mapping over a date restricted string based config to pull the required component function name
  //   - pulling that named function implementation from the registry.
  //   - if logging is enabled, we're wrapping that function implementation in a logger function.
  //   - recreating the algorithm component segment to avoid mutating the original in the registry
  let algorithmSegments = dateRestrictedAlgorithmConfiguration.map((configFunctionName) => {
    let algorithmSegment = getAlgorithmSegmentByName(algorithmSegmentRegistry, configFunctionName);
    if (verbose) {
      algorithmSegment.functionImplementation = loggingWrappedSegment(algorithmSegment.functionImplementation);
    }

    return createAlgorithmSegment(
      configFunctionName,
      algorithmSegment.functionImplementation,
      algorithmSegment.parameterFilter
    );
  });

  return algorithmSegments;
};

const reducer = (accumulator, algorithmSegment) => {
  const factorName = algorithmSegment.algorithmComponentName;
  const calculationResult = algorithmSegment.functionImplementation(accumulator);
  return { factorName: factorName, calculationResult: calculationResult };
};

const loggingWrappedReducer = (accumulator, currentValue) => {
  const retVal = reducer(accumulator, currentValue);
  console.log(retVal.calculationResult, `After Applying factor ${retVal.factorName}`);
  return retVal.calculationResult;
};

/**
 *
 *
 * @param {*} params
 * @param {*} algorithmSegments
 * @returns
 */
const applyParams = (algorithmSegments, params) => {
  // arguably this could be for each, though that would violate the no side affects rule
  const modifiedFuncs = algorithmSegments.map((segment) => {
    return createAlgorithmSegment(
      segment.algorithmComponentName,
      segment.functionImplementation(segment.parameterFilter(params)),
      segment.parameterFilter
    );
  });
  return modifiedFuncs;
};

/**
 *
 * @param {} factorSets
 * @param {*} curriedAlgorithmSegments
 * @returns
 */
const applyFactorSets = (curriedAlgorithmSegments, factorSets) => {
  let modifiedFuncs = curriedAlgorithmSegments.map((segment) => {
    const singleFactorSet = getFactorSetForSegment(factorSets, segment);

    return createAlgorithmSegment(
      segment.algorithmComponentName,
      segment.functionImplementation(singleFactorSet),
      segment.parameterFilter
    );
  });

  return modifiedFuncs;
};

module.exports = {
  calculateSharePortionByDateAction,
  assembleAlgorithmSegments,
  dryRunSharePortionAction,
};
